#!/bin/bash

mydotfiles=$HOME/fedora-dotfiles

echo "############################################"
echo "############################################"
echo 'Openbox links'
echo "############################################"
echo "############################################"

ln -s $mydotfiles/.config/tint2  ~/.config
ln -s $mydotfiles/.config/rofi  ~/.config
ln -s $mydotfiles/.config/dunst  ~/.config
ln -s $mydotfiles/.config/kitty  ~/.config
ln -s $mydotfiles/.config/obmenu-generator  ~/.config
ln -s $mydotfiles/.config/openbox  ~/.config
ln -s $mydotfiles/.config/sxhkd  ~/.config
ln -s $mydotfiles/.config/geany  ~/.config

ln -s $mydotfiles/.config/obamenu  ~/.config
ln -s $mydotfiles/.config/picom.conf  ~/.config

ln -s $mydotfiles/Wallpapers  ~/Pictures
ln -s $mydotfiles/.themes  ~/.themes
ln -s $mydotfiles/.fonts  ~/.fonts
ln -s $mydotfiles/.icons  ~/.icons

echo "############################################"
echo "############################################"
echo 'Openbox links created'
echo "############################################"
echo "############################################"


echo "###############################################"
echo 'Creating symlinks for bspwm...'
echo "###############################################"

ln -s $mydotfiles/.config/bspwm  ~/.config
ln -s $mydotfiles/.config/dmenu  ~/.config
ln -s $mydotfiles/.config/polybar  ~/.config

echo "############################################"
echo "############################################"
echo 'SYMLINKS CREATED...'
echo "############################################"
echo "############################################"
