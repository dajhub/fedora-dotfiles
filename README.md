# fedora-dotfiles



## Openbox

![Openbox on Fedora](https://i.imgur.com/aZJy4hj.jpg)

This is my setup for Openbox on Fedora.  Assumes a minimimal install of Fedora (https://alt.fedoraproject.org/ - scroll down to 'Everything'): 

Once Fedora is installed, reboot and login to the terminal.  In the terminal install git and then clone the gitlab repository:

```
sudo dnf install git
git clone https://gitlab.com/dajhub/fedora-dotfiles.git

```

Having cloned the repository move into the directory and then run the first script.  Check the script first... you may want to change the host name, browser choice, etc!

```
cd fedora-dotfiles
sudo ./1-openbox-install.sh

```

Final step is to create symlinks to their respective folders.  This can be done through the following script, which does not require sudo privileges.  You may want to edit this file since it will also create symlinks for bspwm as well as openbox.

```
./2-symlink.sh

```

## bspwm

![bspwm on Fedora](https://i.imgur.com/yaLKQ00.jpg)


There are also scripts for installing bspwm.  Assumes that Openbox is already installed (above).  The script installs bspwm, dmenu, and polybar.  In a terminal type:

```
cd fedora-dotfiles
sudo ./3-bspwm-install.sh

```



## Miscellaneous
### 1) DuckDuckGo
Theme for nord - copy and paste:
- 2e3440
  
—-- 
- 434c5e
  
  
—--
- d8dee9
- 88c0d0
  
  
—--
- eceff4
- 81a1c1
- 3b4252

