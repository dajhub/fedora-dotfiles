#!/bin/bash

HOSTNAME=zen

print "\n\e[0;33mSystem setup\e[0m"

# ---

echo 'Set hostname...'

hostnamectl set-hostname "$HOSTNAME"

# ---

echo "###############################################"
echo '# Installing required packages for Openbox...'
echo "###############################################"

sudo dnf install -y \
              Xorg \
              xinit \
              xterm \
              openbox \
              tint2 \
              lightdm-gtk \
              lxappearance \
              lxappearance-obconf \
              dunst \
              jgmenu \
              flatpak \
              sxhkd \
              nitrogen \
              pipewire \
              pavucontrol \
              lxpolkit \
              unzip \
              chromium \
              spacefm \
              thunar \
              thunar-archive-plugin \
              NetworkManager-wifi \
              network-manager-applet \
              volumeicon \
              xfce4-power-manager 

# ---
echo "###############################################"
echo '# Enabling lightdm display manager...'
echo "###############################################"

systemctl enable lightdm; # Enable login using graphical interface
systemctl set-default graphical.target; # Boot to graphical interface as default

# ---

echo "###############################################"
echo '# Installing RPM Fusion & Flathub...'
echo "###############################################"

sudo dnf install -y \
  https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm

sudo dnf install -y \
  https://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm


# ---

echo 'Install Flathub...'

sudo flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo

# ---

echo 'Upgrading DNF...'

sudo dnf upgrade --refresh -y


echo "###############################################"
echo '# RPM Fusion & Flathub Installed...'
echo "###############################################"

# ---

echo "###############################################"
echo '# Installing additional DNF packages.  This may take some time ...'
echo "###############################################"

sudo dnf install \
    slick-greeter \
    light-locker \
    xed \
    geany \
    kitty \
    htop \
    neofetch \
    xfce4-screenshooter \
    viewnior \
    abiword \
    flameshot \
    wget \
    micro \
    catfish \
    tumbler \
    rofi \
    dmenu \
    picom \
    xarchiver \
    task \
    papirus-icon-theme.noarch \
    google-noto-cjk-fonts-common.noarch \
    liberation-fonts \
    liberation-sans-fonts \
    fira-code-fonts \
    jgmenu-pmenu \
    gcolor2 
#   jgmenu \
#   nano \
#   tar \
#   ffmpeg \

echo "###############################################"
echo '# DNF packages installed...'
echo "###############################################"

# ---

#echo "###############################################"
#echo '# Installing Librewolf...'
#echo "###############################################"

#sudo rpm --import https://keys.openpgp.org/vks/v1/by-fingerprint/034F7776EF5E0C613D2F7934D29FBD5F93C0CFC3

#sudo dnf config-manager --add-repo https://rpm.librewolf.net

#sudo dnf install -qy --refresh librewolf

#echo "###############################################"
#echo '# Librewolf installed...'
#echo "###############################################"

# ---

echo "###############################################"
echo '# Installing Openbox Dynamic Root Menu...'
echo "###############################################"

sudo dnf install -y perl-Gtk3.noarch perl-App-cpanminus.noarch

git clone https://github.com/trizen/obmenu-generator.git
sudo cp obmenu-generator/obmenu-generator /usr/bin
sudo cp -r obmenu-generator/schema.pl ~/.config/obmenu-generator
sudo cpanm Linux::DesktopFiles
sudo cpanm Data::Dump
sudo chmod 777 /usr/bin/obmenu-generator

obmenu-generator -p -i

echo "############################################"
echo "############################################"
echo "FINISHED..."
echo "To finish installation type: './2-symlink.sh'"
echo "############################################"
echo "############################################"







